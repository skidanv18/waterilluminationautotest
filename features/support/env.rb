require 'em/pure_ruby'
require 'appium_lib'

def caps
  { caps: {
    deviceName: "Name",
    platformName: "Android",
    app: (File.join(File.dirname(__FILE__), "com.xaxtix.team.waterillunimation_1_apps.evozi.com.apk")),
    appPackage: "com.xaxtix.team.waterillunimation",
    appActivity: "com.xaxtix.team.waterillunination.MainActivity",
    newCommandTimeout: "3600"
  } }
end

Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object

def check_element_by_class(element_class, element_text)
  begin
    check_text(find_element(class: element_class), element_text)
  rescue Selenium::WebDriver::Error::NoSuchElementError
    fail("There is no such element with class \"#{element_class}\". Error: #{error}")
  end
end

def check_element_by_id(element_id, element_text)
  begin
    check_text(find_element(id: element_id), element_text)
  rescue Selenium::WebDriver::Error::NoSuchElementError
    fail("There is no such element with id \"#{element_id}\".")
  end
end

def check_text(element, element_text)
  if element.text != element_text
    fail("Element text is not correct. Actual value: '#{element.text}'. Expected value: '#{element_text}'")
  end
end