Before do
  $driver.start_driver
end

After do |scenario|
  if scenario.failed?
    unless File.directory?("screenshots")
      FileUtils.mkdir_p("screenshots")
    end

    screenshot_file = File.join("screenshots", Time.now.strftime("%Y-%m-%d %H.%M.%S"))
    $driver.screenshot("#{screenshot_file}.png")
  end
  $driver.driver_quit
end

AfterConfiguration do
  FileUtils.rm_r("screenshots") if File.directory?("screenshots")
end