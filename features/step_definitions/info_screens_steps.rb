Given(/^I land on 1 screen$/) do
  check_element_by_id("text", "Вы можете освещать воду")
  check_element_by_id("button", "ДАЛЕЕ")
end

Then(/^I land on 2 screen$/) do
  check_element_by_id("text", "Нажмите кнопку осветить")
  check_element_by_id("button", "ДАЛЕЕ")
end

Then(/^I land on 3 screen$/) do
  check_element_by_id("text", "Погрузите телефон в воду и ждите")
  check_element_by_id("button", "ДАЛЕЕ")
end

Then(/^I land on 4 screen$/) do
  check_element_by_id("text", "ВНИМАНИЕ!
Используйте только водонепроницаемые устройства с поддержкой IP68")
  check_element_by_id("button", "НАЧАТЬ")
end

When(/^I try to swap to next screen$/) do
  Appium::TouchAction.new.swipe(start_x: 0.99, start_y: 0.5, end_x: 0.1, end_y: 0.5, duration: 500).perform
end

When(/^I try to swap to previous screen$/) do
  Appium::TouchAction.new.swipe(start_x: 0.01, start_y: 0.5, end_x: 0.9, end_y: 0.5, duration: 500).perform
end
