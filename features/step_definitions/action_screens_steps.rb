When(/^I go through info screens with buttons$/) do
  4.times {
    find_element(id: "button").click
    sleep(0.5)
  }
end

Then(/^I land on dark water screen$/) do
  check_element_by_class("android.widget.TextView", 'Осветить')
end

When(/^I сlick illuminate button$/) do
  find_element(class: "android.widget.TextView").click
  sleep(15)
end

Then(/^I land on illuminated water screen$/) do
  check_element_by_class("android.widget.ImageView", "")
  check_element_by_class("android.widget.TextView", "Вода освещена
Наслаждайтесь")
end

When(/^I сlick cross button$/) do
  find_element(class: "android.widget.ImageView").click
  sleep(0.5)
end