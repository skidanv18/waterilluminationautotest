Feature: User is able to convert area units

  @info_screens
  Scenario: User is able to move through information screens with swaps
    Given I land on 1 screen
    When I try to swap to next screen
    Then I land on 2 screen
    When I try to swap to next screen
    Then I land on 3 screen
    When I try to swap to next screen
    Then I land on 4 screen
    When I try to swap to next screen
    Then I land on 4 screen
    When I try to swap to previous screen
    Then I land on 3 screen
    When I try to swap to previous screen
    Then I land on 2 screen
    When I try to swap to previous screen
    Then I land on 1 screen
    When I try to swap to previous screen
    Then I land on 1 screen

  @action_screens
  Scenario: User is able to go to the action screens with buttons and use water illumination
    Given I land on 1 screen
    When I go through info screens with buttons
    Then I land on dark water screen
    When I сlick illuminate button
    Then I land on illuminated water screen
    When I сlick cross button
    Then I land on dark water screen



